# gmat2020a_box
This repository contains the files needed to install the DigitalSats GMAT development environment in a Vagrant Virtual Machine (box).
## Purpose
The DigitalSats project provides open source repositories for enhancements to the
NASA Generalized Mission Analysis Tool - [GMAT](https://gmat.atlassian.net/wiki/spaces/GW/overview?mode=global),
using Hashicorp [Vagrant](https://www.vagrantup.com/).
## Environments
The scripts will provide either an Ubuntu 18.04 LTS Vagrant box or an Ubuntu 20.04 LTS Vagrant box.
The scripts in this repository use lsb_release to identify the current Ubuntu release.
## File Summary
* Several installation bash shell scripts - see this repository's Wiki
* Vagrantfile - Vagrant file that defines an Ubuntu LTS Vagrant box, currently 20.04.
## Saved Virtual Machines
Both an Ubuntu 20.04 Vagrant [.box](https://drive.google.com/file/d/1lcJuQMnyGWJKhaJs3_sSx8nnzR105CAu/view?usp=sharing) file and a Virtual Box [.ova](https://drive.google.com/file/d/1kzAvPC-xBoMwWMfVFNYOENtZr8U9fbua/view?usp=sharing) file are available for download from Google Drive.
## Assistance
If you have questions or issues about this repository, please check the [Wiki](https://gitlab.com/digitalsats1/gmat2020a-box/-/wikis/home) or post them in the [Digitalsats Blog](http://blog.digitalsats.com/).
## Modifications
Please take lots of snapshots before and after you modify your box or Vagrantfile.

Networking is configured using Network Address Translation (NAT).
This means that while the virtual machine can access the internet via your host's ip address, connecting to the guest is only available from your host machine.
The available user name is vagrant, with a password of vagrant.
If you change the networking configuration, it is a very good idea to change the vagrant user's password.
### Interface Choices
#### Console
All common VM software provides access to the console.
This is the simplest way to interface with the VMs.
#### SSH
Use the following command:
``` command
ssh vagrant@127.0.0.1 -p 2204
```
#### RDP
Bring up an RDP client and connect to 127.0.0.1:33889
